﻿using UnityEngine;
using UnityEngine.UI;

public class NextLevelUI : MonoBehaviour
{
    public Button NextLevelButton;

    void Awake()
    {
        Hide();
        
        NextLevelButton.onClick.AddListener(() =>
        {
            Hide();
            GameManager.Instance.LoadNextLevel();
        });
    }

    void Update()
    {
        if(GameManager.Instance.IsLevelCompleted())
            Show();
    }

    void Hide()
    {
        NextLevelButton.gameObject.SetActive(false);
    }

    void Show()
    {
        NextLevelButton.gameObject.SetActive(true);
    }
}
