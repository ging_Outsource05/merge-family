﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

[Serializable]
public class Level
{
    public int CharacterCount = 30;
    [AssetsOnly]
    public GameObject[] CharacterPrefabs;
    [AssetsOnly]
    public GameObject Environment;
}

public class GameManager : MonoBehaviour
{
    public Level[] Levels;
    public static GameManager Instance { get; private set; }

    GameObject CurrentEnvironment;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
    static void Initialize()
    {
        Instance = null;
    }
    
    void Awake()
    {
        if(Instance == null)
            Instance = this;
        else 
            Destroy(gameObject);
    }

    void Start()
    {
        LoadLevel();
    }

    void LoadLevel()
    {
        var levelIndex = GetCurrentLevelIndex();
        LoadLevel(levelIndex);
    }

    int GetCurrentLevelIndex()
    {
        return PlayerPrefs.GetInt("Level", 0);;
    }

    void SetLevelIndex(int index)
    {
        PlayerPrefs.SetInt("Level", index);
    }

    void LoadLevel(int levelIndex)
    {
        if(CurrentEnvironment != null)
            Destroy(CurrentEnvironment);

        var level = levelIndex >= Levels.Length ?
            Levels.GetRandom() : 
            Levels[levelIndex];

        CurrentEnvironment = Instantiate(level.Environment);
        CharacterSpawner.Instance.LoadLevel(level);
    }

    [Button]
    public void ResetLevel()
    {
        SetLevelIndex(0);
    }

    [Button]
    public void LoadNextLevel()
    {
        SetLevelIndex(GetCurrentLevelIndex() + 1);
        LoadLevel();
    }

    public bool IsLevelCompleted()
    {
        if(MergeManager.Instance.IsMergeSequenceRunning)
            return false;
        if(!MergeManager.Instance.IsMergePossible())
            return true;

        return CharacterController.AllCharacters.Count == 0;
    }
}
