﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharacterController : MonoBehaviour
{
    public float CharacterSpeed = 5f;
    public float RotationSpeed = 5f;
    public NavMeshAgent Agent;
    public static readonly List<CharacterController> AllCharacters = new List<CharacterController>();

    [NonSerialized]
    public int CharacterID;
    
    void Start()
    {
        SetupAgent();
    }

    public void SetModel(GameObject prefab)
    {
        var model = Instantiate(prefab, transform);
        model.transform.localPosition = Vector3.zero; 
        model.transform.localRotation = Quaternion.identity;
        model.AddComponent<PingPong>();
    }
    
    void SetupAgent()
    {
        Agent.updatePosition = true;
        Agent.updateRotation = false;
        Agent.speed = CharacterSpeed;
    }

    public void Stop()
    {
        Agent.isStopped = true;
    }

    void Update()
    {
        if(Agent.enabled && Util.IsPathCompleted(Agent))
        {
            Util.RandomPointOnNavMesh(CharacterSpawner.CenterPosition, CharacterSpawner.CharacterRange, out var nextPosition);
            Agent.SetDestination(nextPosition);
        }
        else
        {
            var targetRotation = Quaternion.LookRotation(Agent.velocity.normalized);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, RotationSpeed * Time.deltaTime);
        }
        
    }
    
    void OnEnable()
    {
        AllCharacters.Add(this);
    }

    void OnDisable()
    {
        AllCharacters.Remove(this);
    }
}
