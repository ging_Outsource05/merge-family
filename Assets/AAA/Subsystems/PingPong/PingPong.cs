﻿using UnityEngine;

public class PingPong : MonoBehaviour
{
    public float MaxHeight = 2f;
    public float Speed = 5f;
    
    void Update()
    {
        transform.localPosition = new Vector3(
            transform.localPosition.x,
            Mathf.PingPong(Time.time * Speed, MaxHeight),
            transform.localPosition.z);
    }
}
