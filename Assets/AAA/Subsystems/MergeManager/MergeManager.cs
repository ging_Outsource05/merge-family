using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;

public class Merge
{
	public int CharacterID;
	public readonly List<CharacterController> Characters = new List<CharacterController>();
}

public class MergeManager : MonoBehaviour
{
	public float ScaleAnimationDuration = 0.5f;
	public float MergeAnimationDuration = 2f;
	public float MergeDistanceMax = 2.5f;
	public LineRenderer LineRendererPrefab;
	public GameObject MergeFinishedParticles;

	public static MergeManager Instance { get; private set; }
	public bool IsMergeSequenceRunning => MergeSequenceCount > 0;
	int MergeSequenceCount;
	
	Merge CurrentMerge;
	bool IsTouching;
	Vector3 TouchStart;
	Vector3 TouchEnd;
	Camera Camera;
	readonly List<LineRenderer> LineRenderers = new List<LineRenderer>();

	public bool IsMergePossible()
	{
		// Check if characters with same id exists
		var allCharacters = CharacterController.AllCharacters;

		return 
			allCharacters.Count != allCharacters.Select(character => character.CharacterID).Distinct().Count();
	}
	
	[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
	static void Initialize()
	{
		Instance = null;
	}

	void Awake()
	{
		if(Instance == null)
			Instance = this;
		else
			Destroy(gameObject);
	}

	void Start()
	{
		Camera = Camera.main;
	}

	void Update()
	{
		if(
			!IsTouching &&
			Input.GetMouseButtonDown(0))
		{
			IsTouching = true;
			TouchStart = Input.mousePosition;
		}

		if(
			IsTouching &&
			Input.GetMouseButtonUp(0))
		{
			IsTouching = false;
			TouchStart = Vector3.zero;
		}

		if(IsTouching)
		{
			TouchEnd = Input.mousePosition;

			var touchPos = ScreenToWorldPosition(TouchEnd);
			var allCharacters = CharacterController.AllCharacters;

			for(int i = 0; i < allCharacters.Count; i++)
			{
				var character = allCharacters[i];
				var characterPos = character.transform.position;

				if(Vector3.Distance(characterPos, touchPos) < MergeDistanceMax)
				{
					if(CurrentMerge == null)
					{
						CreateNewMerge(character);
					}
					else
					{
						if(CurrentMerge.CharacterID != character.CharacterID)
						{
							EndCurrentMerge();
							CreateNewMerge(character);
						}
						else
						{
							if(CurrentMerge.Characters.Contains(character))
								return;

							CurrentMerge.Characters.Add(character);
						}
					}
				}
			}
		}
		else
		{
			EndCurrentMerge();
		}

		DrawCurrentMerge();
	}

	LineRenderer GetLineRenderer()
	{
		for(int i = 0; i < LineRenderers.Count; i++)
		{
			if(!LineRenderers[i].gameObject.activeSelf)
			{
				LineRenderers[i].gameObject.SetActive(true);
				return LineRenderers[i];
			}
		}

		var newRenderer = Instantiate(LineRendererPrefab);
		LineRenderers.Add(newRenderer);
		return newRenderer;
	}

	void CollectLineRenderers()
	{
		for(int i = 0; i < LineRenderers.Count; i++)
		{
			LineRenderers[i].gameObject.SetActive(false);
		}
	}

	void DrawCurrentMerge()
	{
		CollectLineRenderers();

		if(IsTouching)
		{
			if(CurrentMerge != null)
			{
				var characterCount = CurrentMerge.Characters.Count;
				var size = characterCount + 1;
				var positions = new Vector3[size];

				for(int i = 0; i < characterCount; i++)
				{
					var position = CurrentMerge.Characters[i].transform.position;
					position.y *= 0.5f;
					positions[i] = position;
				}

				positions[size - 1] = ScreenToWorldPosition(TouchEnd);
				var lineRenderer = GetLineRenderer();
				lineRenderer.positionCount = size;
				lineRenderer.SetPositions(positions);
			}
			else
			{
				var lineRenderer = GetLineRenderer();
				lineRenderer.positionCount = 2;

				lineRenderer.SetPositions(new[]
				{
					ScreenToWorldPosition(TouchStart),
					ScreenToWorldPosition(TouchEnd),
				});
			}
		}
	}

	void EndCurrentMerge()
	{
		if(
			CurrentMerge != null &&
			CurrentMerge.Characters.Count > 1)
		{
			StartCoroutine(EndMergeSequence(CurrentMerge.Characters));
		}

		CurrentMerge = null;
	}

	IEnumerator EndMergeSequence(List<CharacterController> characters)
	{
		MergeSequenceCount++;
		var transformArray = characters.Select(character => character.transform).ToArray();

		for(int i = 0; i < characters.Count; i++)
		{
			Destroy(characters[i].Agent);
			Destroy(characters[i]);
		}

		for(int i = 0; i < transformArray.Length - 1; i++)
		{
			var current = transformArray[i];
			var next = transformArray[i + 1];
			current.DOMove(next.position, MergeAnimationDuration);

			yield return new WaitForSeconds(MergeAnimationDuration);

			var combinedScale = current.transform.localScale.x + next.transform.localScale.x;

			var targetPosition = next.position;
			targetPosition.y = combinedScale * 0.5f;

			next.DOScale(Vector3.one * combinedScale, ScaleAnimationDuration);
			next.DOMove(targetPosition, ScaleAnimationDuration);
			Destroy(current.gameObject);

			Util.TriggerHapticFeedback();

			yield return new WaitForSeconds(ScaleAnimationDuration);
		}

		var lastObject = transformArray.Last();

		if(MergeFinishedParticles != null)
		{
			var particle = Instantiate(MergeFinishedParticles, lastObject.position, Quaternion.identity);
			Destroy(particle, 5f);
		}

		Destroy(lastObject.gameObject);
		MergeSequenceCount--;
	}

	void CreateNewMerge(CharacterController character)
	{
		CurrentMerge = new Merge();
		CurrentMerge.CharacterID = character.CharacterID;
		CurrentMerge.Characters.Add(character);
	}

	Ray GetCameraRay(Vector3 screenPos)
	{
		return Camera.ScreenPointToRay(screenPos);
	}

	Vector3 ScreenToWorldPosition(Vector3 screenPos)
	{
		var plane = new Plane(Vector3.up, Vector3.zero);
		var ray = GetCameraRay(screenPos);
		plane.Raycast(ray, out var enter);
		return ray.GetPoint(enter);
	}
}