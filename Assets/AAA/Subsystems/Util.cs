﻿using System.Collections.Generic;
using TapticPlugin;
using UnityEngine;
using UnityEngine.AI;

public static class Util
{
	public static bool RandomPointOnNavMesh(Vector3 center, float range, out Vector3 result)
	{
		for(int i = 0; i < 30; i++)
		{
			var randomVector = Random.insideUnitSphere * range;
			randomVector.y = 0f;
			var randomPoint = center + randomVector;

			if(NavMesh.SamplePosition(randomPoint, out var hit, 1.0f, NavMesh.AllAreas))
			{
				result = hit.position;
				return true;
			}
		}

		result = Vector3.zero;
		return false;
	}

	public static bool IsPathCompleted(NavMeshAgent agent)
	{
		if (!agent.pathPending)
		{
			if (agent.remainingDistance <= agent.stoppingDistance)
			{
				var epsilon = 0.01f;
				
				if (!agent.hasPath || Mathf.Abs(agent.velocity.sqrMagnitude) < epsilon)
				{
					return true;
				}
			}
		}

		return false;
	}

	public static T Last<T>(this T[] arr)
	{
		return arr[arr.Length - 1];
	}

	public static T Last<T>(this List<T> list)
	{
		return list[list.Count - 1];
	}

	public static T GetRandom<T>(this T[] arr)
	{
		return arr[Random.Range(0, arr.Length)];
	}

	public static void TriggerHapticFeedback()
	{
		TapticManager.Impact(ImpactFeedback.Medium);
	}
}
