﻿using UnityEngine;

public class CharacterSpawner : MonoBehaviour
{
	public CharacterController ControllerPrefab;
	public static float CharacterRange = 30f;
	public static Vector3 CenterPosition = Vector3.zero;

	public static CharacterSpawner Instance { get; private set; }

	[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
	static void Initialize()
	{
		Instance = null;
	}

	void Awake()
	{
		if(Instance == null)
			Instance = this;
		else
			Destroy(gameObject);
	}

	public void LoadLevel(Level level)
	{
		for(int i = 0; i < CharacterController.AllCharacters.Count; i++)
		{
			Destroy(CharacterController.AllCharacters[i].gameObject);
		}
		
		for(int i = 0; i < level.CharacterCount; i++)
		{
			CreateNewCharacter(level);
		}
	}

	void CreateNewCharacter(Level level)
	{
		Util.RandomPointOnNavMesh(CenterPosition, CharacterRange, out var position);
		var characterInstance = Instantiate(ControllerPrefab, position, Quaternion.identity);
		
		var index = Random.Range(0, level.CharacterPrefabs.Length);
		var modelPrefab = level.CharacterPrefabs[index];
		characterInstance.SetModel(modelPrefab);
		characterInstance.CharacterID = index;
	}
}