﻿using UnityEngine;

public class TimeManager : MonoBehaviour
{
	public float TimeScaleMin = 0.3f;
	public float TimeScaleAlterSpeed = 2f;

	const float TimeScaleMax = 1f;

	void Update()
	{
		if(
			Input.GetMouseButton(0))
		{
			if(Time.timeScale >= TimeScaleMin)

				Time.timeScale = Mathf.Clamp(Time.timeScale - TimeScaleAlterSpeed * Time.deltaTime, TimeScaleMin,
					TimeScaleMax);
		}
		else
		{
			if(Time.timeScale <= TimeScaleMax)
			{
				Time.timeScale = Mathf.Clamp(Time.timeScale + TimeScaleAlterSpeed * Time.deltaTime, TimeScaleMin,
					TimeScaleMax);
			}
		}
	}
}